chrome-to-mpv
=============

## About

This is a small Bash/Applescript combo that will open the current YouTube clip in [mpv player](https://mpv.io/). If no YouTube link is present in the current tab, the script will also check clipboard for such.


## Disclaimer

There are [more](https://github.com/Thann/play-with-mpv) [efficient](https://github.com/agiz/youtube-mpv) [ways](https://github.com/r3dbU7z/play-it-mpv) to achieve that.
